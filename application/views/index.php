<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>PPDB Online</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="ppdb smk wikrama 1 jepara" name="keywords">
  <meta content="Pendaftaran Peserta Didik Baru 2019-2020 SMK Wikrama 1 Jepara" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url();?>assets/img/Logo_swiksara.png" rel="icon">
  <link href="<?php echo base_url();?>assets/img/Logo_swiksara.png" rel="icon">

  <!-- Google Fonts -->
  

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url();?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url();?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="#intro" class="scrollto"><img src="<?php echo base_url();?>assets/img/logo.png" width="50%"></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container" style="margin-top: 20px;">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Home</a></li>
          <li><a href="#about">Tentang Wikrama</a></li>
          <li><a href="#services">Persyaratan</a></li>
          <li><a href="<?php echo site_url('Mastercontrol/fpd');?>">Pendaftaran</a></li>
          <li><a href="#contact">Kontak Kami</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active">
            <div class="carousel-background"><img src="<?php echo base_url();?>assets/img/1.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h3 style="color: #fff;font-weight: bold;">PENERIMAAN PESERTA DIDIK BARU</h3>
                <h3 style="color: #fff;">MAN 1 PATI</h3>
                <h3 style="color: #fff;">TAHUN PELAJARAN 2020-2021</h3>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="<?php echo base_url();?>assets/img/2.jpg" alt=""></div>
            <div class="carousel-container">
             <div class="carousel-content">
                <h3 style="color: #fff;font-weight: bold;">PENERIMAAN PESERTA DIDIK BARU</h3>
                <h3 style="color: #fff;">MAN 1 PATIA</h3>
                <h3 style="color: #fff;">TAHUN PELAJARAN 2020-2021</h3>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="<?php echo base_url();?>assets/img/3.jpg" alt=""></div>
            <div class="carousel-container">
             <div class="carousel-content">
                <h3 style="color: #fff;font-weight: bold;">PENERIMAAN PESERTA DIDIK BARU</h3>
                <h3 style="color: #fff;">MAN 1 PATI</h3>
                <h3 style="color: #fff;">TAHUN PELAJARAN 2020-2021</h3>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="<?php echo base_url();?>assets/img/4.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h3 style="color: #fff;font-weight: bold;">PENERIMAAN PESERTA DIDIK BARU</h3>
                <h3 style="color: #fff;">SMK WIKRAMA 1 JEPARA</h3>
                <h3 style="color: #fff;">TAHUN PELAJARAN 2020-2021</h3>
              </div>
            </div>
          </div>

          

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 box" style="background-color: #3598db;">
            <i class="ion-ios-stopwatch-outline" style="color:#fff;"></i>
            <h4 class="title"><a href="">Gelombang 1</a></h4>
            <p class="description">Pendaftaran : 25 Februari 2019 - 29 Maret 2019</p>
            <p class="description">Pengumuman  : 01 April 2019</p>
          </div>

          <div class="col-lg-4 box box-bg" style="background: #f87f52;">
            <i class="ion-ios-stopwatch-outline" style="color:#fff;"></i>
            <h4 class="title"><a href="">Gelombang 2</a></h4>
            <p class="description">Pendaftaran : 02 April 2019 - 22 Juni 2019</p>
            <p class="description">Pengumuman  : 24 Juni 2019</p>
          </div>

          <div class="col-lg-4 box" style="background-color: #1abc9c;">
            <i class="ion-ios-stopwatch-outline" style="color:#fff;"></i>
            <h4 class="title"><a href="">Gelombang 3</a></h4>
            <p class="description">-</p>
          </div>

        </div>
      </div>
    </section><!-- #featured-services -->

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3 style="border-color: #3498db;">Tentang Wikrama</h3>
          <p>Ilmu yang Amaliah | Amal yang Ilmiah | Akhlakul Karimah</p>
        </header>

        <div class="row">
          <div class="col-lg-4 box" style="background-color: #337ab7;height: 300px;">
            
            <h3 style="color: #fff;font-size: 20px;margin-top: 50px;margin-left: 10px;">KOMPETENSI KEAHLIAN</h3>
            <div class="jdl" style="border-bottom: 2px solid #fff;margin-left:10px;width: 100px;margin-top: -5px;">
            </div>
            <p style="color: #fff;margin-top: 20px;margin-left: 10px;">1. Rekayasa Perangkat Lunak</p>
            <p style="color: #fff;margin-left: 10px;">2. Teknik Komputer dan Jaringan</p>
          </div>
          <div class="col-lg-4 box">
            <a href="<?php echo base_url();?>assets/img/rpl.jpg"><img src="<?php echo base_url();?>assets/img/rpl.jpg" class="img-responsive" style="width:100%;height: 300px;">
          </div>

          <div class="col-lg-4 box">
            <a href="<?php echo base_url();?>assets/img/tkj.jpg"><img src="<?php echo base_url();?>assets/img/tkj.jpg" class="img-responsive" style="width:100%;height: 300px;"></a>
          </div>

        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-7" style="background-color: #000;">
                        <iframe width="100%" height="380" src="https://www.youtube.com/embed/PJg5Jlpxp4g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      </div>
                      <div class="col-lg-5" style="background-color: #e9c706;height: 400px;">
                        <h3 style="color: #fff;font-size: 20px;margin-top: 50px;margin-left: 10px;">PROFIL SEKOLAH</h3>
                        <div class="jdl" style="border-bottom: 2px solid #fff;margin-left:10px;width: 100px;margin-top: -5px;">
                        </div>
                      </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
          <div class="col-lg-5" style="background-color: #337ab7;height: 400px;">
            <h3 style="color: #fff;font-size: 20px;margin-top: 50px;margin-left: 10px;">APA KATA ALUMNI ?</h3>
            <div class="jdl" style="border-bottom: 2px solid #fff;margin-left:10px;width: 100px;margin-top: -5px;">
            </div>
            <p style="color: #fff;margin-left: 10px;margin-top: 20px;">Padamu Negeri Kami Berjanji, Lulus Wikrama Siap Membangun Negeri</p>
            <p style="color: #fff;margin-left: 10px;">Sekolahnya OKE, Gurunya OKE, Siswanya OKE, Orang Tuanya OKE dan Alumninya OKE OKE</p>
          </div>
          <div class="col-lg-7" style="background-color: #000;">
             <iframe width="100%" height="380" src="https://www.youtube.com/embed/ClXbv7B0hMw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>

        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <h3>Persyaratan</h3>
          <p>Apa saja ya persyaratan yang diperlukan ?</p>
        </header>

        <div class="row">

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
            <h4 class="title"><a>Fotocopy KTP ortu 1 lembar</a></h4>
            <p class="description">Penyerahan dokumen dilakukan setelah anda berhasil mendaftar di SMK Wikrama 1 Jepara, setiap hari Senin - Jumat pukul 07.00 - 16.00 WIB</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
            <h4 class="title"><a>Fotocopy akta kelahiran</a></h4>
            <p class="description">Penyerahan dokumen dilakukan setelah anda berhasil mendaftar di SMK Wikrama 1 Jepara, setiap hari Senin - Jumat pukul 07.00 - 16.00 WIB</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-paper-outline"></i></div>
            <h4 class="title"><a>Fotocopy KK</a></h4>
            <p class="description">Penyerahan dokumen dilakukan setelah anda berhasil mendaftar di SMK Wikrama 1 Jepara, setiap hari Senin - Jumat pukul 07.00 - 16.00 WIB</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
            <h4 class="title"><a >Fotocopy raport semester 1-5</a></h4>
            <p class="description">Penyerahan dokumen dilakukan setelah anda berhasil mendaftar di SMK Wikrama 1 Jepara, setiap hari Senin - Jumat pukul 07.00 - 16.00 WIB</p>
          </div>
        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeIn">
      <div class="container text-center">
        <h3>Ayo buruan daftar !!</h3>
        <p>Untuk pendaftaran secara online bisa klik link diabawah ini</p>
        <a class="cta-btn" href="<?php echo site_url('Mastercontrol/fpd');?>">Daftar Sekarang</a>
      </div>
    </section><!-- #call-to-action -->

    <!--==========================
      Skills Section
    ============================-->

    <!--==========================
      Facts Section
    ============================-->

    <!--==========================
      Portfolio Section
    ============================-->

    <!--==========================
      Clients Section
    ============================-->

    <!--==========================
      Clients Section
    ============================-->
    <!--==========================
      Team Section
    ============================-->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>Kontak Kami</h3>
          <p>Untuk informasi yang terkait bisa menghubungi kontak dibawah ini</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Alamat</h3>
              <address>Jl. Kelet Ploso Km. 36 Keling - Jepara 54954</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>No Hp / WA</h3>
              <p><a>082313070561 (Bu Zia)</a></p>
              <p><a>082377680498 (Pak Arik)</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a>smkwikrama1jepara@gmail.com</a></p>
            </div>
          </div>

        </div>
      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    

    <div class="container">
      
      <div class="copyright">
        <div class="social-links">
        <a href="https://www.facebook.com/smkwikrama1jepara" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.instagram.com/swiksara/" class="instagram"><i class="fa fa-instagram"></i></a>
        <a href="https://www.youtube.com/channel/UCRz3ziJqOKg4JN1TyQL8Kpg" class="youtube"><i class="fa fa-youtube"></i></a>
      </div>
      <div style="margin-top: 20px;">
        &copy; Copyright <strong>SMK Wikrama 1 Jepara</strong>. All Rights Reserved
      </div>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/superfish/hoverIntent.js"></script>
  <script src="<?php echo base_url();?>assets/lib/superfish/superfish.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/lightbox/js/lightbox.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url();?>assets/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url();?>assets/js/main.js"></script>
  <style type="text/css">
    .ktk{
      width: 350px;
      height: 300px;
      background-color: #000;
    }
  </style>
</body>
</html>
